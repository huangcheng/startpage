import type { Translations } from './';

const en: Translations = {
  AVATAR: 'Avatar',
  CANCEL: 'Cancel',
  CATEGORIES: 'Categories',
  CATEGORY: 'Category',
  EMAIL: 'Email',
  LOGIN: 'Login',
  LOGIN_SUCCESS: 'Login Success',
  LOGOUT: 'Logout',
  LogOUT_SUCCESS: 'Logout Success',
  NAVIGATION_ITEM: 'Navigation Item',
  NAVIGATION_ITEMS: 'Navigation Items',
  NICKNAME: 'Nickname',
  PASSWORD: 'Password',
  PASSWORD_IS_INCORRECT: 'Password is Incorrect',
  PERSONAL_INFORMATION: 'Personal Information',
  PERSONAL_INFORMATION_MODIFY_SUCCESS: 'Personal Information Modify Success',
  PERSONAL_SETTINGS: 'Personal Settings',
  PLEASE_ENTER_YOUR_PASSWORD: 'Please Enter Your Password',
  PLEASE_ENTER_YOUR_USERNAME: 'Please Enter Your Username',
  SAVE: 'Save',
  USERNAME_OR_PASSWORD_IS_INCORRECT: 'Username or Password is Incorrect',
  WELCOME_TO_LOGIN: 'Welcome to Login',
};

export default en;
