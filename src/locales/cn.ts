import type { Translations } from './';

const cn: Translations = {
  AVATAR: '头像',
  CANCEL: '取消',
  CATEGORIES: '分类',
  CATEGORY: '分类',
  EMAIL: '邮箱',
  LOGIN: '登录',
  LOGIN_SUCCESS: '登录成功',
  LOGOUT: '退出登录',
  LOGOUT_SUCCESS: '退出成功',
  NAVIGATION_ITEM: '导航项目',
  NAVIGATION_ITEMS: '导航项目',
  NICKNAME: '昵称',
  PASSWORD: '密码',
  PASSWORD_IS_INCORRECT: '密码错误',
  PERSONAL_INFORMATION: '个人信息',
  PERSONAL_INFORMATION_MODIFY_SUCCESS: '个人信息修改成功',
  PERSONAL_SETTINGS: '个人设置',
  PLEASE_ENTER_YOUR_PASSWORD: '请输入密码',
  PLEASE_ENTER_YOUR_USERNAME: '请输入用户名',
  SAVE: '保存',
  USERNAME_OR_PASSWORD_IS_INCORRECT: '用户名或密码错误',
  WELCOME_TO_LOGIN: '欢迎登录',
};

export default cn;
